<?php
namespace App\Model;
/**
 * 
 * @author HP
 *
 */
class Database_config
{

    protected static $db;
    protected static $errno;
    protected static $errstr;
    protected static $errfile;
    protected static $errline;

    public function __construct()
    {
        try {
            static::$db = new \PDO('mysql:host=localhost;dbname=ql_ban_sua', 'root', '');
            static::$db->exec("set names utf8");
        } // Catch any errors
catch (PDOException $e) {
            echo $e->getMessage();
            exit();
        }
    }
/**
 * 
 * @param unknown $sql
 * @return string|unknown
 */
    public function fetch($sql)
    {
        $result = static::$db->query($sql);
        $arr = "";
        while ($row = $result->fetchAll(\PDO::FETCH_ASSOC)) {
            $arr = $row;
        }
        return $arr;
    }
/**
 * 
 * @param unknown $errno
 * @param unknown $errstr
 * @param unknown $errfile
 * @param unknown $errline
 */
    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        $this->$errno = $errno;
       $this->$errstr= $errstr; 
        $query = "INSERT INTO errorlog (severity, message, filename, lineno,time) VALUES (?, ?, ?, ?, NOW())";
        $stmt = static::$db->prepare($query);
        switch ($errno) {
            case E_NOTICE:
            case E_USER_NOTICE:
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
            case E_STRICT:
                $stmt->execute(array(
                    "NOTICE",
                    $errstr,
                    $errfile,
                    $errline
                ));
                break;
            case E_WARNING:
            case E_USER_WARNING:
                $stmt->execute(array(
                    "WARNING",
                    $errstr,
                    $errfile,
                    $errline
                ));
                break;
            case E_ERROR:
            case E_USER_ERROR:
                $stmt->execute(array(
                    "FATAL",
                    $errstr,
                    $errfile,
                    $errline
                ));
                exit("FATAL error $errstr at $errfile:$errline");
            default:
                exit("Unknown error at $errfile:$errline");
        }
    }
}